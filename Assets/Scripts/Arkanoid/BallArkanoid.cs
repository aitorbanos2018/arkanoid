﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallArkanoid : MonoBehaviour
{
    public Vector2 ballVelocity;

    void FixedUpdate()
    {
        transform.Translate(ballVelocity.x*Time.deltaTime, ballVelocity.y*Time.deltaTime, 0);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Muro")
        {
            Debug.LogWarning("Hemos chocado contra un muro");
            ballVelocity.x*=-1;
        }
        else if (collision.gameObject.tag == "MuroFin")
        {
            Debug.LogWarning("Hemos chocado contra el muro final");
            ballVelocity.y*=-1;
        }else if (collision.gameObject.tag == "Player")
        {
            Debug.LogWarning("Hemos chocado contra el player");
            ballVelocity.y*=-1;
        }else if (collision.gameObject.tag == "Block")
        {
            Debug.LogWarning("Chocamos con bloque");
            ballVelocity.y*=-1;
            collision.gameObject.GetComponent<BlockArkanoid>().TouchBall();
        }

    }
}
