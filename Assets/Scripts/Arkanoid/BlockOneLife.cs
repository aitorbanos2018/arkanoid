﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockOneLife : BlockArkanoid
{
    protected override void Start(){
        lives=1;
        Colorear();
    }
 
    protected override void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float r,g,b;
        r = 1.0f;
        g = 0;
        b = 0;
 
        sp.color = new Color(r,g,b,1f);
    }
}