﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockThreeLife : BlockArkanoid
{
    protected override void Start(){
        lives=3;
        Colorear();
    }
 
    protected override void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float r,g,b;
        r = 0;
        g = 1.0f;
        b = 0;
 
        sp.color = new Color(r,g,b,1f);
    }
}